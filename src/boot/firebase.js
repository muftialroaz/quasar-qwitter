import { initializeApp } from "firebase/app";
import { getFirestore, collection, getDocs } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyC6OHEanSjUlxfVBFEjEf6sqw0Wy9EGY7g",
  authDomain: "qwitter-1ebef.firebaseapp.com",
  projectId: "qwitter-1ebef",
  storageBucket: "qwitter-1ebef.appspot.com",
  messagingSenderId: "268059619412",
  appId: "1:268059619412:web:a9efe294f90646af023dc1",
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export default db;
