# Qwitter (qwitter)

A Quasar Framework app. Qwitter (Twitter Clone App.) App project from Youtube Tutorial.

## Install the dependencies

```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Build the app for production

```bash
quasar build
```

### Demo Site

```bash
https://quasar-qwitter-muftialroaz.vercel.app
```


### Customize the configuration

See [Configuring quasar.conf.js](https://v2.quasar.dev/quasar-cli/quasar-conf-js). <br>
See [Youtube: FreeCodeCamp](https://www.youtube.com/watch?v=la-0ulfn0_M) for the tutorial.
